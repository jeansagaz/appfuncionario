@extends('index')

	@section('content')

		<div class="container">

			<script src="../../js/jquery-1.11.3.min.js"></script>
			<script src="../../js/jquery.validate.min.js"></script>
			<script src="../../js/formValid.js"></script>

			<h1>Editar Funcionario: {{$funcionario->nome}}</h1>

			@if ($errors->any())
				<ul class="alert alert-warning">
					@foreach($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			@endif

			{!! Form::open(['url'=>"funcionario/update/$funcionario->id", 'method'=>'put', 'id'=>'form', 'files'=>true]) !!}

				<!-- Nome Form Input -->
				<div class="form-group">
					{!! Form::label('nome', 'Nome:') !!}
					{!! Form::text('nome', $funcionario->nome, ['class'=>'form-control']) !!}
				</div>

				<!-- E-mail Form Input -->
				<div class="form-group">
					{!! Form::label('email', 'E-mail:') !!}			
					{!! Form::text('email', $funcionario->email, ['class'=>'form-control']) !!}
				</div>

				<!-- Setor Form Input -->
				<div class="form-group">
					{!! Form::label('setor', 'Setor:') !!}			
					{!! Form::text('setor', $funcionario->setor, ['class'=>'form-control']) !!}
				</div>
				
				<!-- Foto Form Input -->
				<div class="form-group">
					{!! Form::label('foto', 'Foto:') !!}
					{!! Form::label('foto', $funcionario->foto) !!}					
					{!! Form::file('foto', null) !!}
  					<p class="errors">{!!$errors->first('foto')!!}</p>

  					@if(Session::has('error'))
						<p class="errors">{!! Session::get('error') !!}</p>
					@endif

  				</div>

				<div class="form-group">
					{!! Form::submit('Salvar Funcionário', ['class'=>'btn btn-primary']) !!}
				</div>

			{!! Form::close() !!}
			<p><a href="/" title="Voltar">Voltar</a></p>

		</div>

	@endsection