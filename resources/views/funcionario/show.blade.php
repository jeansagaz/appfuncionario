@extends('index')

	@section('content')

		<div class="container">
			<h1>Visualizar Funcionario: {{$funcionario->nome}}</h1>

			@if ($errors->any())
				<ul class="alert alert-warning">
					@foreach($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			@endif

			{!! Form::open(['url'=>"funcionario/delete/$funcionario->id", 'method'=>'delete']) !!}

				<!-- Nome Form Input -->
				<div class="form-group">
					{!! Form::label('nome', 'Nome:') !!}
					{!! Form::text('nome', $funcionario->nome, ['class'=>'form-control']) !!}
				</div>

				<!-- E-mail Form Input -->
				<div class="form-group">
					{!! Form::label('email', 'E-mail:') !!}			
					{!! Form::text('email', $funcionario->email, ['class'=>'form-control']) !!}
				</div>

				<!-- Setor Form Input -->
				<div class="form-group">
					{!! Form::label('setor', 'Setor:') !!}			
					{!! Form::text('setor', $funcionario->setor, ['class'=>'form-control']) !!}
				</div>

				<!-- Foto Form Input -->
				<div class="form-group">
					{!! Form::label('foto', 'Foto:') !!}			
					{!! Form::label('foto', $funcionario->foto, ['class'=>'form-control']) !!}

					<?php $img = !empty($funcionario->foto) ? $funcionario->foto : 'user.png' ?>
					<img src="../../uploads/{{$img}}" alt="{{$img}}" title="{{$img}}" class="img-thumbnail">
				</div>				

				<div class="form-group">
					{!! Form::submit('Apagar Funcionário', ['class'=>'btn btn-primary']) !!}
				</div>

			{!! Form::close() !!}
			<p><a href="/" title="Voltar">Voltar</a></p>
		</div>

	@endsection