@extends('index')

	@section('content')

		<div class="container">

			<script src="../../js/jquery-1.11.3.min.js"></script>
			<script src="../../js/jquery.validate.min.js"></script>
			<script src="../../js/formValid.js"></script>
		
			<h1>Novo Funcionário</h1>

			@if ($errors->any())
				<ul class="alert alert-warning">
					@foreach($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			@endif

			{!! Form::open(array('url'=>'funcionario/createPost', 'files'=>true, 'id'=> 'form')) !!}

				<!-- Nome Form Input -->
				<div class="form-group">
					{!! Form::label('nome', 'Nome:') !!}
					{!! Form::text('nome', null, ['class'=>'form-control']) !!}
				</div>

				<!-- E-mail Form Input -->
				<div class="form-group">
					{!! Form::label('email', 'E-mail:') !!}
					{!! Form::text('email', null, ['class'=>'form-control']) !!}
				</div>

				<!-- Setor Form Input -->
				<div class="form-group">
					{!! Form::label('setor', 'Setor:') !!}
					{!! Form::text('setor', null, ['class'=>'form-control']) !!}
				</div>

				<!-- Foto Form Input -->
				<div class="form-group">
					{!! Form::label('foto', 'Foto:') !!}
					{!! Form::file('foto', null) !!}
  					<p class="errors">{!!$errors->first('foto')!!}</p>

  					@if(Session::has('error'))
						<p class="errors">{!! Session::get('error') !!}</p>
					@endif

  				</div>

				<div class="form-group">
					{!! Form::submit('Novo Funcionário', ['class'=>'btn btn-primary']) !!}
				</div>

			{!! Form::close() !!}
			<p><a href="/" title="Voltar">Voltar</a></p>

		</div>
		

	@endsection