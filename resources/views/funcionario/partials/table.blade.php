<table class="table table-striped table-hover">
	<tr>
		<th>Nome</th>
		<th>E-mail</th>
		<th>Setor</th>
		<th>Foto</th>
		<th>Ações</th>
	</tr>

	@foreach($funcionarios as $funcionario)
		<tr>
			<td>{{$funcionario->nome}}</td>
			<td>{{$funcionario->email}}</td>
			<td>{{$funcionario->setor}}</td>
			<td>
				<?php $img = !empty($funcionario->foto) ? $funcionario->foto : 'user.png' ?>
				<img src="uploads/{{$img}}" alt="{{$img}}" title="{{$img}}" class="img-thumbnail" width="100" height="100">
			</td>
			<td>
				<a href="funcionario/edit/{{$funcionario->id}}" class="btn btn-info" title="Editar">
					<span class="glyphicon glyphicon-pencil"></span>
				</a>&nbsp
				<a href="funcionario/show/{{$funcionario->id}}" class="btn btn-primary" title="Visualizar">
					<span class="glyphicon glyphicon-search"></span>
				</a>
			</td>
		</tr>
	@endforeach

</table>