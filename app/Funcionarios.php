<?php

namespace appFuncionarios;

use Illuminate\Database\Eloquent\Model;

class Funcionarios extends Model
{
    //
	protected $table = 'funcionarios';

    protected $fillable = ['nome', 'email', 'setor', 'foto'];
}
