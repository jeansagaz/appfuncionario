<?php

namespace appFuncionarios\Http\Controllers;

use Illuminate\Http\Request;

use appFuncionarios\Http\Requests;

use appFuncionarios\Funcionarios;
use appFuncionarios\Http\Requests\FuncionarioRequest;

use Validator;
use Session;
use File;
use Input;

class FuncionarioController extends Controller
{
	private $fileName;
	private $errors;

	public function index(Request $request)
	{
		$name = $request->get('name', '');
		if(!empty($name))
			$funcionarios = Funcionarios::where('nome', 'LIKE', '%'.$name.'%')->get();
		else
			$funcionarios = Funcionarios::all();

		return view('funcionario.index',['funcionarios'=>$funcionarios, 'name'=>$name]);
	}

	public function show($id)
	{
		$funcionario = Funcionarios::find($id);
		return view('funcionario.show', compact('funcionario'));
	}

	public function create()
 	{
 		return view('funcionario.create');
 	}

 	public function createPost(FuncionarioRequest $request)
	{
		if ($request->file('foto') != null) {
			
			if($this->uploadFile($request->file('foto'))) {

			    $funcionario = array(
			      'nome' => $request->get('nome'),
			      'email'  => $request->get('email'),
			      'setor'  => $request->get('setor'),
			      'foto' => $this->fileName
			    );

			}
			else {
				
			    Session::flash('error', $this->errors['error']);			    
			    return view('funcionario.create');

			}	    	
	    	
		}
		else {			

		    $funcionario = array(
		      'nome' => $request->get('nome'),
		      'email'  => $request->get('email'),
		      'setor'  => $request->get('setor')
		    );

		}

		Funcionarios::create($funcionario);

		return redirect('/');
	}	

	public function edit($id)
	{
		$funcionario = Funcionarios::find($id);
		return view('funcionario.edit', compact('funcionario'));
	}

	public function update($id, FuncionarioRequest $request)
	{		
		$funcionario = Funcionarios::find($id);
		
		if ($request->file('foto') != null) {
			
			//apgaga a foto antiga caso exista
			$this->filename = public_path().'\\uploads\\'.$funcionario->foto;
			if (File::exists($this->filename)) {
				File::delete($this->filename);
			}

			//faz o upload da nova foto
			if($this->uploadFile($request->file('foto'))) {				

			    $funcionario->nome = $request->get('nome');
			    $funcionario->email = $request->get('email');
			    $funcionario->setor = $request->get('setor');
			    $funcionario->foto = $this->fileName;

			}
			else {			
				
			    Session::flash('error', $this->errors['error']);			    
			    return view('funcionario.edit.'.$id);

			}	    	
	    	
		}
		else {

		    $funcionario->nome = $request->get('nome');
			$funcionario->email = $request->get('email');
		    $funcionario->setor = $request->get('setor');		    

		}
		
		$funcionario->update();

		return redirect('/');
	}

	public function delete($id)
	{
		$funcionario = Funcionarios::find($id);
		
		if (!empty($funcionario->foto)) {

			$this->filename = public_path().'\\uploads\\'.$funcionario->foto;			
			if (File::exists($this->filename)) {
				File::delete($this->filename);
			}

		}

		$funcionario->delete();

		return redirect('/');
	}

	private function uploadFile($paramFile)
	{		
  		$file = array('image' => $paramFile);
  		// setting up rules
  		$rules = array('image' => 'required',); //mimes:jpeg,bmp,png and for max size max:10000
  		// doing the validation, passing post data, rules and the messages
  		$validator = Validator::make($file, $rules);
  		if ($validator->fails()) {

  			// send back to the page with the input data and errors
    		$this->errors = array('error' => 'Not an image.', 'code' => 400);

    		return false;
  		}
  		else {

    		// checking file is valid.
  			if ($file['image']->isValid()) {

      			$destinationPath = 'uploads'; // upload path      			
      			$extension = $file['image']->getClientOriginalExtension(); // getting image extension
      			//$fileName = rand(11111,99999).'.'.$extension; // renameing image
      			//$fileName = str_replace(' ', '', $file['image']->getClientOriginalName()).'.'.$extension;
      			$this->fileName = str_replace(' ', '', $file['image']->getClientOriginalName());
      			
      			$file['image']->move($destinationPath, $this->fileName); // uploading file to given path	
      			
      			return true;
    		}
    		else {

      			// sending back with error message.      			
      			$this->errors = array('error' => 'uploaded file is not valid', 'code' => 400);
      			
      			return false;
    		}

  		}
	}

}
