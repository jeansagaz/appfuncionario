<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*Route::get('/', function () {
  	return view('welcome');
});*/


Route::get('/','FuncionarioController@index');

Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@logout');

Route::controllers([
	'auth' => 'Auth\AuthController',
 	'password' => 'Auth\PasswordController',
]);

Route::group(['prefix'=> 'funcionario'], function() {
	Route::get('/','FuncionarioController@index');

	//chama o formulário do funcionário
	Route::get('create','FuncionarioController@create');
	//salva o funcionário
	Route::post('createPost','FuncionarioController@createPost');

	//editando funcionário
	Route::get('edit/{id}','FuncionarioController@edit');
	Route::put('update/{id}','FuncionarioController@update');

	//apaga o funcionário
	Route::delete('delete/{id}','FuncionarioController@delete');

	//exibe o funcionário
	Route::get('show/{id}','FuncionarioController@show');
});

Route::group(['middleware' => ['web']], function () {
    //
});

Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/home', 'HomeController@index');
});

Route::get('profile/{user}', function(User $user) {
    return $user;
});