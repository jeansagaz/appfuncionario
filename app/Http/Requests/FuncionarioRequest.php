<?php

namespace appFuncionarios\Http\Requests;

use appFuncionarios\Http\Requests\Request;

class FuncionarioRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required|min:5',
            'email' => 'required|email',
            'setor'=> 'required',
            'foto' => 'mimes:jpg,jpeg,|max:10000'
        ];
    }
}
